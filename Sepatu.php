<?php

class Sepatu{
    public $merk;
    public $size = 41;
    public $model = 'canvas';

    public function __construct($nama){
    
        $this->merk = $nama;    
    }

    public function setSize($size){

        $this->size = $size;
    }

    public function setModel ($data){

        $this->model = $data;
    }

    public function getSize(){

        return $this->size;
    }

    public function getModel(){

        return $this->model;
    }
}

?>